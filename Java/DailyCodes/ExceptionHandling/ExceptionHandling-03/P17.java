import java.util.*;
class P17{
	static void fun(){
		System.out.println("Start fun");
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number a : ");
		int a = sc.nextInt();
		System.out.print("Enter number b : ");
		int b = sc.nextInt();
		try{
			System.out.println(a/b);
		}catch(Exception e){
			System.out.println("number b should not be 0.Please try again.");
			System.out.print("Enter number b : ");
			b = sc.nextInt();
			System.out.println(a/b);
		}
		System.out.println("End fun");
	}
	public static void main(String[] args){
 		System.out.println("Start Main");
		fun();
		System.out.println("End Main");
	}

}
