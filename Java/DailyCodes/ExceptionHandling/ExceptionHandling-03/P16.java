import java.util.*;
class P16{
	static void fun(){
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		System.out.println("Start fun");
		try{
			System.out.println(a/b);
		}catch(ArithmeticException e){
			System.out.println("Second input should not be zero.");
		}
		System.out.println("End fun");
	}
	public static void main(String[] args){
		System.out.println("Start Main");
		fun();
		System.out.println("End Main");

	}
}
