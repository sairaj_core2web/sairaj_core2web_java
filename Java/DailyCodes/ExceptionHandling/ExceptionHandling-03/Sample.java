import java.util.*;
class Sample{
	static void fun(){
		Scanner sc = new Scanner(System.in);
		int a = 0,b = a;
		try{
			System.out.print("Enter val a : ");
			a = sc.nextInt();
			System.out.print("Enter val b : ");
			b = sc.nextInt();
			System.out.println(a/b);
		}catch(InputMismatchException e){
			System.out.println("Input mismatch exception");
		}
		catch(ArithmeticException e){
			System.out.println("Zero division exception");
		}
	}
	public static void main(String[] args){
		System.out.println("Start main");
		fun();
		System.out.println("End main");
	}

}
