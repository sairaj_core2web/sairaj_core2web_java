import java.util.*;
class P19{
	static void fun(){
		Scanner sc = new Scanner(System.in);
		try{
			System.out.print("Enter val a : ");
			int a = sc.nextInt();
			System.out.print("Enter val b : ");
			int b = sc.nextInt();
		}catch(InputMismatchException e){
			System.out.println("Input mismatch exception");
		}
		try{
			System.out.println(a/b);
		}catch(ArithmeticException e){
			System.out.println("Zero division exception");
		}
	}
	public static void main(String[] args){
		System.out.println("Start main");
		fun();
		System.out.println("End main");
	}

}
