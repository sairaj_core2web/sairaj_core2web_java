import java.util.*;
class P18{
	static void fun(){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number a : ");
		int a = sc.nextInt();
		System.out.print("Enter number b : ");
		int b = sc.nextInt();
		System.out.println("Start fun");
		try{
			System.out.println(a/b);
		}catch(ArithmeticException ae){
			System.out.print("Wrong input.Enter value of b again : ");
			b = sc.nextInt();
			try{
				System.out.println(a/b);
			}catch(ArithmeticException ar){
				System.out.println(ar);
			}
		}
		System.out.println("End fun");
	}
	public static void main(String[] args){
		System.out.println("Start main");
		fun();
		System.out.println("End main");
	}
}
