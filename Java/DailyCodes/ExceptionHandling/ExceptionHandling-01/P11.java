/*	
* 	There are two ways to handle an exception : 1.try/catch & 2.throws keyword
*	Here as we have not handled the exception,thus the exception is handled by JVM's Default Exception Handler.
*	Format of default exception handler:
*	1.Thread name
*	2.Exception name
*	3.Description (optional)
*	4.Stacktrace
*/
class P11{
	static void fun(int x,int y){
		System.out.println("Start fun");
		System.out.println(x/y);
		System.out.println("End fun");
	}
	public static void main(String[] args){

		int x = 10;
		int y = 0;

		System.out.println("Start code");
		fun(x,y);
		System.out.println("End code");
	
	}
}
