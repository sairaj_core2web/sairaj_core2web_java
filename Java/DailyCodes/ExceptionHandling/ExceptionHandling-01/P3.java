import java.io.*;
class P3{		
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String name = br.readLine();
		//System.out.println(name.charAt(3));	//Allowed
		System.out.println(name[3]);	//Not allowed : error:- Array required, String found
	}
}
